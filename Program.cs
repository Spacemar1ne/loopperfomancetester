﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace LoopsPerfomanceTester
{
    class Program
    {
        static CancellationTokenSource cts = new CancellationTokenSource();

        static void Main(string[] args)
        {
            GC.Collect();
            Task t = GloabalStuff();
            Console.ReadKey();
            cts.Cancel();
            t.Wait();
        }

        static Task GloabalStuff()
        {
            return Task.Run(() =>
            {
                TimeSpan workingTime = TimeSpan.FromMinutes(20);
                ContinueWithTest tester;
                bool val;
                tester = new ContinueWithTest();
                tester.DoStuff();
                val = cts.Token.WaitHandle.WaitOne(workingTime);
                tester.Stop();
            });
        }
    }
}
