﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace LoopsPerfomanceTester
{
    public class ContinueWithTest
    {
        CancellationTokenSource cts = new CancellationTokenSource();

        private void ProcessAsyncStuff(Task t)
        {
            Console.WriteLine(" Assync Stuff ");
            Thread.Sleep(2*1000);
        }

        private Task DoStuffAsync()
        {
           return Task.Run(() => { Console.WriteLine(DateTime.Now.TimeOfDay + " Stuff"); }, cts.Token);
        }

        public void DoStuff()
        {
            Task.Run(async () =>
            {
                try
                {
                    while (!cts.IsCancellationRequested)
                        await DoStuffAsync_3();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }, cts.Token);
        }

        private async Task DoStuffAsync_3()
        {
            await DoStuffAsync().ContinueWith(ProcessAsyncStuff, cts.Token);
        }


        public void Stop()
        {
            cts.Cancel();
        }
    }
}
